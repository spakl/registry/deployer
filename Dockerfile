FROM registry.gitlab.com/spakl/common-utils:v1.0.0 as skutils
FROM hashicorp/terraform:1.7.3 as tf 
FROM hashicorp/vault:1.15 as vault

FROM ubuntu:mantic
ENV DEBIAN_FRONTEND=noninteractive

COPY --from=skutils /skutils /skutils
# Install prerequisites
RUN apt-get update && apt-get --no-install-recommends -y install \
    jq \
    curl \
    unzip \
    bash \
    make \
    ca-certificates && \
    apt-get clean && apt-get autoclean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY --from=tf /bin/terraform /usr/local/bin/terraform 
COPY --from=vault /bin/vault /usr/local/bin/vault 

RUN ["/bin/bash", "-c", "source /skutils/index.sh && /skutils/scripts/tls/install_ca.sh"]

SHELL ["/bin/bash", "-c"]